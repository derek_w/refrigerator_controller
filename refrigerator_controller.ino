/*
  All temperatures in Degrees C.
*/

// Adafruit RGB LCD Shield Library - Version: 1.0.1
#include <Adafruit_RGBLCDShield.h>
#include <Adafruit_MCP23017.h>
#include <EEPROM.h>
#include <Wire.h>

#define EEPROM_VALID_BYTE 0x0
#define EEPROM_DATA_VALID 0x55
#define EEPROM_SET_POINT_BYTE 0x1
#define EEPROM_MIN_FIN_TEMPERATURE_BYTE 0x2

// The Adafruit RGB LCD shield uses the I2C SCL and SDA pins. On classic Arduinos
// this is Analog 4 and 5 so you can't use those for analogRead() anymore
// However, you can connect other I2C sensors to the I2C bus and share
// the I2C bus.
Adafruit_RGBLCDShield lcdAdafruit = Adafruit_RGBLCDShield();

// Pin connections
const int finTempSensor = A0;
const int roomTempSensor = A1;

// Resistance of thermistor at 25 degrees C.
#define THERMISTOR_NOMINAL 10000
// Temperature for nominal resistance.
#define TEMPERATURE_NOMINAL 25
// Beta coefficient of the thermistor.
#define B_COEFFICIENT 3950
// Value of series resistor.
#define SERIES_RESISTOR 20000

const int heaterPwm = 3; // Digital pin 3 has PWM capabilities

const int ledPin = 13;

const int DEFAULT_SET_POINT = 5; // ~41F on roomTemperatureSensor
const int DEFAULT_MIN_FIN_TEMPERATURE = 2;
const int AVERAGE_COUNT = 5; // Take 5 readings and average them.
const int HYSTERESIS = 1; // ~5 degrees F
const unsigned long DEFROST_DELAY = 300000; // ~5 minute delay
const byte DEFAULT_HEATER_DUTY_CYCLE = 128; // 50% Duty cycle

int roomTemperature;
int finTemperature;
int setPoint;
int minFinTemperature;
int sensorValue;
byte heaterDutyCycle;
byte currentHeaterDutyCycle;
bool acEnabled;
bool finTempDisplay = false;

unsigned long currentMillis = 0;
unsigned long previousMillis = 0;

unsigned long defrostCount = 0;

void setup()
{
   Serial.begin( 9600 );

   if( EEPROM.read( EEPROM_VALID_BYTE ) == EEPROM_DATA_VALID )
   {
      Serial.println( "Using EEPROM Set Temperatures" );
   }
   else
   {
      // Set the current setPoint to default, 41 degrees F
      EEPROM.write( EEPROM_SET_POINT_BYTE, DEFAULT_SET_POINT );
      EEPROM.write( EEPROM_MIN_FIN_TEMPERATURE_BYTE, DEFAULT_MIN_FIN_TEMPERATURE );
      // Set 0 byte to 55 indicating EEPROM has temperature settings
      EEPROM.write( EEPROM_VALID_BYTE, EEPROM_DATA_VALID );
   }

   setPoint = EEPROM.read( 0x1 );
   minFinTemperature = EEPROM.read( 0x2 );

   // Initialize the LCD with 16 characters and 2 lines.
   lcdAdafruit.begin( 16, 2 );

   // Draw labels on the screen
   drawScreen( finTempDisplay );

   // Set heaterPwm and ledPin pins as outputs.
   pinMode( heaterPwm, OUTPUT );
   pinMode( ledPin, OUTPUT );

   // Start with the AC off.
   acEnabled = false;

   // Start with displaying the fin temperature
   finTempDisplay = true;

   // Set the heater duty cycle to default.
   heaterDutyCycle = DEFAULT_HEATER_DUTY_CYCLE;
}

void loop()
{
   currentMillis = millis();

   // Update EEPROM Set Values only once every 10 seconds
   // and only if they change.
   if( ( currentMillis - previousMillis ) > 10000 )
   {
      Serial.println( "Update EEPROM Values" );
      EEPROM.update( EEPROM_SET_POINT_BYTE, setPoint );
      EEPROM.update( EEPROM_MIN_FIN_TEMPERATURE_BYTE, minFinTemperature );
      previousMillis = currentMillis;
   }

   // Check to see if any keypad buttons are pressed, and update the setpoint accordingly.
   checkButtons();

   // Read the finTemperature and roomTemperature sensors.
   ReadSensors();

   if( ( acEnabled ) && ( roomTemperature <= 25 ) && ( roomTemperature > setPoint ) )
   {
      // 50% duty cycle to keep the AC sensor warm with a 100 ohm 3W @ 12V (~1.44W)
      HeaterSet( heaterDutyCycle );
      digitalWrite( ledPin, HIGH );
      Serial.println( "Heater On" );
   }
   else
   {
      // Turn heater off
      HeaterSet( 0 );
      digitalWrite( ledPin, LOW );
      Serial.println( "Heater Off" );
   }

   if( ( roomTemperature <= setPoint ) || ( finTemperature <= minFinTemperature ) )
   {
      acEnabled = false; // Turn off the AC. Setpoint reached or finTemperature low.
      HeaterSet( 0 ); // Turn off the heater.
      digitalWrite( ledPin, LOW ); // Turn off LED.
      Serial.println( "Setpoint Reached or finTemperature below minFinTemperature" );
   }

   if( finTemperature <= minFinTemperature )
   {
      defrostCount = DEFROST_DELAY;
      Serial.println( "finTemperature below minFinTemperature" );

      // If fins are too cold, delay until they aren't any more.
      while( finTemperature <= minFinTemperature )
      {
         // ~10 seconds before we re-read the finTemperature.
         for( int i = 0; i < 100; i++ )
         {
            // ~0.1 sec per pass until above or equal to minFinTemperature.
            delay( 100 );
            // Update the setpoint if it gets changed.
            checkButtons();
            ReadSensors();
         }
         Serial.print( "finTemperature still below minFinTemperature" );
         Serial.println( "Delay 10 Seconds" );
      } 
   }

   // If the AC is not on, check if it should be turned on.
   // This is where the hysteresis control is implemented: must warm to HYSTERESIS above SetPoint
   if( ( !acEnabled ) && ( ( roomTemperature >= ( setPoint + HYSTERESIS ) ) && ( finTemperature > 2 ) ) )
   {
      acEnabled = true;
   }
}

// Set and retain the heater dutyCycle.
void HeaterSet( byte dutyCycle )
{
   analogWrite( heaterPwm, dutyCycle );
   // Save the current value each time it is set.
   currentHeaterDutyCycle = dutyCycle;
}

void ReadSensors()
{
   // Shut off the heater before taking analog readings and restore the heater value after taking 
   // readings for noise reduction. Previous value will be restored after sensors have been read.
   HeaterSet( 0 );
   // Give the ADC time to settle before taking readings.
   delay( 10 );

   float steinhartValue;
   float thermistorResistance;

   float finTemperatureAdc = 0;
   for( int count = 0; count < AVERAGE_COUNT; count++ )
   {
      finTemperatureAdc = finTemperatureAdc + analogRead( finTempSensor );
      delay( 10 );
   }
   // Average the AVERAGE_COUNT readings.
   finTemperatureAdc = finTemperatureAdc / AVERAGE_COUNT;

   // Calculate the thermistor resistance.
   thermistorResistance = 1023 / finTemperatureAdc - 1 ;
   thermistorResistance = SERIES_RESISTOR / thermistorResistance;

   // Calculate the temperature in Fahrenheit using steinhart equation.
   steinhartValue = thermistorResistance / THERMISTOR_NOMINAL;
   steinhartValue = log( steinhartValue );
   steinhartValue /= B_COEFFICIENT;
   steinhartValue += 1.0 / ( TEMPERATURE_NOMINAL + 273.15 );
   steinhartValue = 1.0 / steinhartValue;
   steinhartValue -= 273.15;
   finTemperature = (int)( steinhartValue );

   float roomTemperatureAdc = 0;
   for( int count = 0; count < AVERAGE_COUNT; count++ )
   {
      roomTemperatureAdc = roomTemperatureAdc + analogRead( roomTempSensor );
      delay( 10 );
   }
   // Average the AVERAGE_COUNT readings.
   roomTemperatureAdc = roomTemperatureAdc / AVERAGE_COUNT;

   // Calculate the thermistor resistance.
   thermistorResistance = 1023 / roomTemperatureAdc - 1 ;
   thermistorResistance = SERIES_RESISTOR / thermistorResistance;
   // Calculate the temperature in Fahrenheit using steinhart equation.
   steinhartValue = thermistorResistance / THERMISTOR_NOMINAL;
   steinhartValue = log( steinhartValue );
   steinhartValue /= B_COEFFICIENT;
   steinhartValue += 1.0 / ( TEMPERATURE_NOMINAL + 273.15 );
   steinhartValue = 1.0 / steinhartValue;
   steinhartValue -= 273.15;
   roomTemperature = (int)( steinhartValue );

   // Restore PWM value.
   HeaterSet( currentHeaterDutyCycle );
}

void checkButtons()
{
   updateSetPoint();
   updateScreen();
}

void drawScreen( bool finTempDisplay )
{
   lcdAdafruit.clear();

   // Start drawing on first line.
   lcdAdafruit.setCursor( 0, 0 );
   lcdAdafruit.print( "Room:" );
   lcdAdafruit.setCursor( 9, 0 );
   lcdAdafruit.print( "Set:" );

   // Change display to either finTemperature or finMinTemperature
   if( finTempDisplay == false )
   {
      // Continue drawing on second line.
      lcdAdafruit.setCursor( 0, 1 );
      lcdAdafruit.print( "Min: " );
      lcdAdafruit.setCursor( 9, 1 );
      lcdAdafruit.print( "AC=" );
   }
   else
   {
      // Continue drawing on second line.
      lcdAdafruit.setCursor( 0, 1 );
      lcdAdafruit.print( "Fins:" );
      lcdAdafruit.setCursor( 9, 1 );
      lcdAdafruit.print( "AC=" );
   }

}

void updateSetPoint()
{
   uint8_t buttonState = lcdAdafruit.readButtons();
   if( buttonState & BUTTON_UP)
   {
      if( setPoint < 25 )
      {
         // Add 1 degree C to setPoint.
         Serial.println( "Increase Setpoint" );
         setPoint++;
      }
   }

   if( buttonState & BUTTON_DOWN)
   {
      if( setPoint > 2 )
      {
         // Subtract 1 degree C from setPoint.
         Serial.println( "Decrease Setpoint" );
         setPoint--;
      }
   }

   if( buttonState & BUTTON_RIGHT)
   {
      if( minFinTemperature < 25 )
      {
         // Add 1 degree C to minFinTemperature.
         Serial.println( "Increase minFinTemperature" );
         minFinTemperature++;
      }
   }

   if( buttonState & BUTTON_LEFT)
   {
      if( minFinTemperature > 0 )
      {
         // Subtract 1 degree C from minFinTemperature.
         Serial.println( "Decrease minFinTemperature" );
         minFinTemperature--;
      }
   }

   if( buttonState & BUTTON_SELECT)
   {
      // Change what is displayed and redraw the screen
      finTempDisplay = !finTempDisplay;
      drawScreen( finTempDisplay );
   }
}

void updateScreen()
{
   // Start drawing on first line.
   lcdAdafruit.setCursor( 5, 0 );
   lcdAdafruit.print( "   " );
   lcdAdafruit.setCursor( 5, 0 );
   //lcdAdafruit.print( roomTemperature );
   lcdAdafruit.print( celsiusToFahrenheit( roomTemperature ) );

   lcdAdafruit.setCursor( 13, 0 );
   lcdAdafruit.print( "   " );
   lcdAdafruit.setCursor( 13, 0 );
   //lcdAdafruit.print( setPoint );
   lcdAdafruit.print( celsiusToFahrenheit( setPoint ) );

   // Continue drawing on second line.
   lcdAdafruit.setCursor( 5, 1 );
   lcdAdafruit.print( "   " );
   lcdAdafruit.setCursor( 5, 1 );
   if( finTempDisplay == false )
   {
      //lcdAdafruit.print( minFinTemperature );
      lcdAdafruit.print( celsiusToFahrenheit( minFinTemperature ) );
   }
   else
   {
      //lcdAdafruit.print( finTemperature );
      lcdAdafruit.print( celsiusToFahrenheit( finTemperature ) );
   }
   lcdAdafruit.setCursor( 12, 1 );
   if( acEnabled )
   {
      lcdAdafruit.print( "ON " );
      lcdAdafruit.setBacklight( 0x4 ); // Blue
   }
   else
   {
      lcdAdafruit.print( "OFF" );
      lcdAdafruit.setBacklight( 0x7 ); // White
   }

}

int celsiusToFahrenheit( int temperatureCelsius )
{
   float temperatureF = ( (float)temperatureCelsius * 9.0 / 5.0 ) + 32.0;
 
   return (int)temperatureF;
}